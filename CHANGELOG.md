# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.11.12 - 2025-01-02(12:14:21 +0000)

### Other

- [mod-dmext] Add a version-agnostic IP range validator

## Release v0.11.11 - 2024-11-21(12:37:59 +0000)

### Other

- - [mod-dmex] set_object_ref doesn't remove the dot

## Release v0.11.10 - 2024-10-25(11:20:43 +0000)

### Other

- - [dhcpv4-server] make dhcpv4 server work with mod-dmext
- - [dhcpv4-server] make dhcpv4 server work with mod-dmext

## Release v0.11.9 - 2024-10-22(08:56:36 +0000)

### Other

- Firewall fails to start when container restarts

## Release v0.11.8 - 2024-09-25(18:16:40 +0000)

### Other

- [mod-dmext] Extend is_valid_ipv4_network to support CSV and SSV strings

## Release v0.11.7 - 2024-09-17(09:51:57 +0000)

### Other

- Firewall fails to start when container restarts

## Release v0.11.6 - 2024-09-09(15:32:26 +0000)

### Other

- Passing invalid priv data pointer

## Release v0.11.5 - 2024-09-03(16:16:15 +0000)

### Other

- [mod-dmext] Add a hostname validator function

## Release v0.11.4 - 2024-09-02(15:50:39 +0000)

### Other

- [mod-dmext] Add a hostname validator function

## Release v0.11.3 - 2024-09-02(07:06:45 +0000)

### Other

- [mod-dmext] Add a string length validation function

## Release v0.11.2 - 2024-09-02(06:59:58 +0000)

### Other

- [mod-dmext] Add unit tests for IPv4 subnet validator function

## Release v0.11.1 - 2024-08-01(09:51:42 +0000)

### Other

- [Security]Server side validation on Wireguard

## Release v0.11.0 - 2024-05-07(16:03:01 +0000)

### New

- Add write action to strip dot from paths

## Release v0.10.1 - 2024-03-08(11:32:26 +0000)

### Other

- [AMX] Implement rotation for when a defined max of instances is reached

## Release v0.10.0 - 2024-03-05(09:11:29 +0000)

### New

- Add some extra functions in mod-dmext to read/write values directly in files.

## Release v0.9.1 - 2023-12-15(15:20:11 +0000)

### Fixes

- set_object_ref no longer accepts empty strings

## Release v0.9.0 - 2023-12-12(11:41:16 +0000)

### New

- [mod-dmext]Improve and extend object reference functionality

## Release v0.8.0 - 2023-12-07(10:45:33 +0000)

### New

- CLONE - [Data model] Add (X_PRPL-COM_)WINSServer parameter in DHCPv4 Server Pool: part 1, provide DataModel

## Release v0.7.2 - 2023-12-06(14:07:02 +0000)

### Fixes

- Fix license headers in files

## Release v0.7.1 - 2023-12-06(13:41:20 +0000)

### Fixes

- [TR181 LED] Defaults are not loaded when log level is set via CLI

## Release v0.7.0 - 2023-12-05(12:34:51 +0000)

### New

- Add posibility for action handlers to provide a description

## Release v0.6.0 - 2023-05-04(12:05:52 +0000)

## Release v0.5.1 - 2023-04-07(14:22:30 +0000)

### Other

- - [amx][mod-dmext] Implement function csv_values_matches_regexp

## Release v0.5.0 - 2023-01-25(09:46:40 +0000)

### New

- [AMX] Write generic function for setting default parameter values

## Release v0.4.1 - 2022-12-13(07:33:36 +0000)

### Fixes

- Parameter with object reference to the same data model causes timeout

## Release v0.4.0 - 2022-10-28(13:35:55 +0000)

### New

- Add validator for check_is_empty_or_in

## Release v0.3.4 - 2022-06-02(06:53:15 +0000)

### Fixes

- Refactor parameter with object references validation

## Release v0.3.3 - 2022-05-23(11:54:10 +0000)

### Fixes

- Object paths in reference parameters  must not end with a dot

## Release v0.3.2 - 2022-05-23(07:46:32 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.3.1 - 2022-02-09(12:42:22 +0000)

### Fixes

- [prpl][qos] After a save(), the QoS plugin does not start anymore

## Release v0.3.0 - 2021-12-10(21:11:53 +0000)

### New

- Add validator for IPv6 prefixes

## Release v0.2.0 - 2021-12-06(16:08:47 +0000)

### New

- Implement reference following to local object

### Other

- Move component from ambiorix to core on gitlab.com

## Release v0.1.4 - 2021-09-14(14:33:10 +0000)

## Release v0.1.3 - 2021-09-14(14:05:40 +0000)

### Fixes

- [amxc] add ifdef around when_null_x, when_str_empty_x

## Release v0.1.2 - 2021-06-15(09:27:54 +0000)

## Release v0.1.1 - 2021-06-15(05:24:49 +0000)

## Release v0.1.0 - 2021-06-14(13:39:21 +0000)

### New

- Add tr-181 ordered instance functionality

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

## Release v0.0.4 - 2021-04-30(14:19:35 +0000)

### Changes

- Modules and plugins should not have a version extension in their names

## Release v0.0.3 - 2021-04-29(17:00:09 +0000)

### Changes

- Disable tests
- Prepare for opensource

## Release 0.0.2 - 2021-04-12(10:03:47 +0000)

### Changed

- forgotten " in BAF file [change]
- Optimization, when bus_ctx is returned with who has function, the object exists
- Migrate to new licenses format (baf)
- Allow empty value for regexp matching

## Release 0.0.1 - 2020-02-04(07:47:00 +0000)

### New

- Initial release
