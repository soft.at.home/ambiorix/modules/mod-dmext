# Module Data Model Extension

[[_TOC_]]

## Introduction

In the [Broadband Forum TR-181 data model definition](https://usp-data-models.broadband-forum.org/tr-181-2-12-0-usp.html) some data types are defined that are not supported by the Ambiorix libraries. Some of these types are `strings` that can only contain certain values, like IPv4 address, MAC address, IPv6 address, ...

As these types can be represented by a string that must match a certain regular expression, a parameter validator function can be written and used throughout the data model.

In many places the TR-181 data model contains references to other objects, these references, often called `Path Name`, are string parameters that contain the absolute object path to an other object, or parameter. The value of these parameters must often be reset to an empty string when the referenced object is deleted.

It is the goal of the data model extension module to provide these functionalities or other common TR-181 requirements. 

By importing this module in the your data model definition, through the main odl file, you can use the provided functions in your odl files.

## Building, Installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

Please read [Getting Started](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md) to create a local workspace and to set-up a local development environment.

More information can be found at:

- [Debug & development environment](https://gitlab.com/prpl-foundation/components/ambiorix/dockers/amx-sah-dbg/-/blob/main/README.md) to set-up a local development environment.

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc) - Generic C api for common data containers
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp) - Common patterns implementations
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo) - The ODL compiler library
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd) - Data model C API


### Build mod-dm-extension

1. Clone the git repository

    To be able to build it, you need the source code. So clone this git-repo to your workspace

    ```bash
    mkdir -p ~/development/amx
    cd ~/development/amx
    git clone git@gitlab.com:prpl-foundation/components/core/modules/mod-dmext.git
    ``` 

1. Install dependencies

    Although the container will contain all tools needed for building, it does not contain the libraries needed for building. Install all libraries needed. When you created the local workspace and followed the [Getting Started](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/getting-started/-/blob/main/README.md) guide, everything should be installed.

    As an alternative it is possible to installed pre-compiled packages in the development container using:

    ```bash
    sudo apt update
    sudo apt install libamxc libamxd libamxp 
    ```

1. Build it

    ```bash
    cd ~/development/amx/mod-dmext
    make
    ```

1. Install it

    When the build is successful, install the binary at the correct location.

    ```bash
    sudo -E make install
    ```

## Using module data model extension

### Import the module

In your main odl file `import` the shared object of this module.

```odl
%config {
    name = "test";

    import-dbg = true;

    // main files
    definition_file = "${name}_definition.odl";
}

import "mod-dmext.so";

include "${definition_file}";
```

After including it, all data model extension functions will be available for use.

### Parameter Validation Functions

All provided functions (action implementations) only work on parameters. Using them on objects will have no effect.

#### Regular expression matching

Function name: <br>

- `matches_regexp <REGEXP STRING>`

Should only be used on `string` parameters.

This function takes one argument which must be a valid regular expression (POSIX Extended).
When the value of the parameter is changed it must match the regular expression, if it is not matching it is an invalid value.

Example:

```odl
object MyObject {
    string OUI = "000000" {
        on action validate call matches_regexp "^[0-9A-F]{6}$";
    }
}
```
Function name: <br>

- `csv_values_matches_regexp <REGEXP STRING>`

Should only be used on `csv_string` parameters.

This function takes one argument which must be a valid regular expression (POSIX Extended).
When the value of the parameter is changed each comma separated values must match the regular expression, if one is not matching it is an invalid value.

Example:

```odl
object MyObject {
    csv_string AllowedPath = "Device.Path.1.,Device.Path.2." {
        on action validate call csv_values_matches_regexp "^(Device\.)?(Path\..+\.)$";
    }
}
```
#### Check Param is empty or in

Validation function check_is_empty_or_in extends amxd_action_param_check_is_in and allows empty values.

Example:

```odl
object MyObject {
    csv_string SupportedControllers = "mod-1, mod-2";

    string Controller = "mod-1" {
        on action validate call check_is_empty_or_in "MyObject.SupportedControllers";
    }
}
```

#### Check Param is empty or equal to

`check_is_empty_or_length_equal_to` can be used on string type parameters (string, csv_string, ssv_string) and verifies if the newly set value (string) is empty or equal in length to the specified value. This validation action takes one value, the length against which to compare (integer).

Example:

```odl
%define {
    object MyObject {
        string Generic = "" {
            on action validate call check_is_empty_or_length_equal_to 33;
        }
    }
}
```

#### IP Address Validation

Function name: <br>

- `is_valid_ipv4` - Validates whether the provided value or values constitute a valid IPv4 address.
- `is_valid_ipv6` - Validates whether the provided value or values constitute a valid IPv6 address
- `is_valid_ip` - Validates whether the provided value or values constitute a valid IP address, can be IPv4 or IPv6
- `is_valid_ipv6_prefix` - Validates whether the provided value or values constitute a valid IPv6 prefix
- `is_valid_macaddr` - Validates whether the provided value or values constitute a valid MAC address
- `is_valid_ipv4_network` - Validates whether the provided value or values constitute a valid IPv4 subnet
- `is_valid_ip_range` - Validates whether the provided value or values constitute a valid IP range (v4/v6 individual address or prefix)
- `is_valid_hostname` - Validates whether the provided value or values constitute a valid hostname

The above validation functions can be used on `string`, `csv_string` or `ssv_string` parameters. In the case of comma-separated or space-separated values, the validation action is applied to each individual value. The validation fails if at least one value is incorrect.

None of these functions take any arguments. When the value of the parameters is changed, the new value will be validated, if it is not valid, the value is not accepted.

Empty (string) values are accepted as well.

Example:

```odl
%define {
    object Test {
        string IPv4Address = "255.255.255.255" {
            on action validate call is_valid_ipv4;
        }
        string IPv6Address = "::1" {
            on action validate call is_valid_ipv6;
        }
        string IPAddress = "0.0.0.0" {
            on action validate call is_valid_ip;
        }
        string IPv6Address = "fedd::/64" {
            on action validate call is_valid_ipv6_prefix;
        }
        string MACAddress = "00:11:22:AA:BB:CC" {
            on action validate call is_valid_macaddr;
        }
        ssv_string MACAddress = "00:11:22:AA:BB:CC 00:11:22:AA:BB:DD" {
            on action validate call is_valid_macaddr;
        }
        csv_string MACAddress = "00:11:22:AA:BB:CC,00:11:22:AA:BB:DD" {
            on action validate call is_valid_macaddr;
        }
        string IPv4Subnet = "192.168.1.1/24" {
            on action validate call is_valid_ipv4_network;
        }
        string IPRange = "10.0.1.1" {
            on action validate call is_valid_ip_range;
        }
        csv_string IPRange = "10.0.1.1,172.0.16.1/16" {
            on action validate call is_valid_ip_range;
        }
        string Hostname = "localhost" {
            on action validate call is_valid_hostname;
        }
    }
}
```

#### Logical Expression Validation

When more complex validation needs to be done on a parameter value, a logical expression can be used. A logical expression evaluates to true or false.

The validation method that can check a logical expression is called `check_expression` and takes as argument a logical expression.

The parameter value is represented in the logical expression as `{value}`

Example:
```
%define {
    object MyObject {
        uint32 Number = 50 {
            on action validate call check_expression 
                "({value} >= 48 && {value} <= 57) || ({value} >= 65 && {value} <= 90)";
        }
    }
}
```

In the above example the accepted value must be in the range `[48,57]` or `[65, 90]`

### Parameters With Object References

Some parameters in the TR-181 (USP) data model contain references to other objects.

Read the parameter documentation in [TR181-USP](https://usp-data-models.broadband-forum.org/tr-181-2-16-0-usp.html) carefully to check which references are allowed in the parameter, often the possible references are limited.

Each parameter containing a reference or a list of references can be defined in the data model using a `string` type or a `csv_string` type.
To limit the possible references that can be set as the parameter value it is recommended to use a regular expression validation.

Example `Device.­User­Interface.­HTTPAccess.­{i}.Interface`

In this parameter the referenced object must either be an interface defined in `Device.IP.Interface.{i}` or an interface defined in `Device.Logicical.Interface.{i}` and is defined as a string with maximum length of 256 bytes.

This parameter can be defined as follows in an odl file:

```
    string Interface {
        on action validate call check_maximum_length 256;
        on action validate call matches_regexp "^$|^Device\.(IP|Logical)\.Interface\.([^\.]*|\[.*\])+\.?$";
    }
```

This will allow the value to be
- Empty
- Any object path the starts with `Device.` followed with `IP` or `Logical` followed with `.Interface.`. 
- The instance can be an index number, the object name (Alias) or a key addressing expression and can be terminated with a dot.
- The length of the provided path must be smaller or equal to 256 characters. 

Examples of valid paths:

- `Device.IP.Interface.2.`
- `Device.Logical.Interface.[Alias=="lan"]`
- `Device.IP.Interface.wan.`

Examples of invalid paths:

- `Device.Users.User.1.`
- `Device.Logical.Interface.1.Stats.`
- `Device.IP.Interface.[Alias=="wan"].IPv4Address.1.`

To check that the provided path is referencing an existing object the `check_is_object_ref` validation action handler can be used. 

```
    string Interface {
        on action validate call check_maximum_length 256;
        on action validate call matches_regexp "^$|^Device\.(IP|Logical)\.Interface\.([^\.]*|\[.*\])+\.?$";
        on action validate call check_is_object_ref;
    }
```
Search paths (which can resolve to multiple instances), wildcard paths or parameter paths as a reference are not supported and are considered invalid.

Key addressing paths are allowed (these are search paths that only use key parameters and returns 0 or exact 1 matching object)

#### Object Reference Action Handlers

Depending on the need it is possible to add other object reference parameter action handlers, the following parameter action handlers are available:

- Validation action - `check_is_object_ref` - checks if the new value is a valid object path and referencing an existing object.
- Write action - `set_object_ref` - sets the object reference path, will fail if the object that is referenced by the path doesn't exists. This write action handler will take a subscription to monitor the referenced object, when the referenced object is removed from the data model, the reference will be removed.
- Write action - `set_normalized_object_ref` - sets the object reference path, will fail if the object that is referenced by the path doesn't exists.
- Write action - `set_object_ref_simple` - will only strip the terminating dot from the reference path.
- Read action - `read_object_ref` - returns the value of the parameter, when one of the object reference action handlers provide transformation data this read handler will provide the transformed path when reading in "private" access mode.
- Destroy action - `destroy_object_ref` - remove the taken subscription if any, this action handler is mandatory when `set_object_ref` is used.

Comma separated object paths are supported, all entries in the comma separated list will be considered individually.

---
> **NOTE**<br>
> The `set_object_ref` and `set_normalized_object_ref` will "normalize" the provided path. It will be translated into an object path with index addressing and will remove any trailing dot.<br>
> According to USP specifications reference parameter must contain an object path without the terminating dot.
---

#### Possible Use Cases

Different use cases can be defined when using object referencing:

Here a list of possible use cases is provided, but is not limiting the number of use cases, other use cases could be possible:

1. Only verify that the path is valid, no normalization needed:

   This is the simplest use case, and basically the only thing required is the use of a regular expression validation funtion.<br>
   Example:
   ```
   string Interface {
       on action validate call check_maximum_length 256;
       on action validate call matches_regexp "^$|^Device\.(IP|Logical)\.Interface\.([^\.]*|\[.*\])+$";
   }
   ```
   No check is done if the referenced object exists, the referenced object is not monitored. The stored path is not normalized, it is stored as it is provided, it must match the provided regular expression. In the provided example any object path that is either referencing an instance of `Device.IP.{i}` or `Device.Logical.{i}` is accepted and can be in index addressing notation or in key addressing notation.

1. Get the stored path without the `Device.` prefix.

   To be compliant with TR-181 specifications the `Device.` prefix must be in the reference path, but often this data model dependency is not needed in the implementation itself. Using path transformation and the read action handler `read_object_ref` the `Device.` data model dependency can be removed.<br>
   Example:
   ```
   string Interface {
       on action validate call check_maximum_length 256;
       on action validate call matches_regexp "^$|^Device\.(IP|Logical)\.Interface\.([^\.]*|\[.*\])+$";
       on action read call read_object_ref  { "'Device.IP.'" = "IP.", "'Device.Logical.'" = "Logical." };
   }
   ```
   In above example when reading the value of the parameter in private access mode the transformed path is returned instead of the stored path.

1. Verify that the path is valid and exists, store the normalized path:

   If it is required that when the value is set it must reference an existing object path, but should be kept when the referenced object is deleted the action handlers `check_is_object_ref` and `set_normalized_object_ref` can be used.<br>
   Example:
   ```
   string Interface {
       on action validate call check_maximum_length 256;
       on action validate call matches_regexp "^$|^Device\.(IP|Logical)\.Interface\.([^\.]*|\[.*\])+\.?$";
       on action validate call check_is_object_ref;
       on action write call set_normalized_object_ref;
   }
   ```
   This will ensure that at the time the value is set the referenced object exists, if it doesn't exist the new value will not be accepted.
   
   ---
   > **WARNING**<br>
   > When this is used on persistent parameters it could be that when the referenced object is deleted the saved data (the object path) can not be loaded again, as it will not be accepted when the referenced object doesn't exist.
   ---

1. Verify that the path is valid and exists, store the normalized path and monitor the referenced object:

   Parameters which contain hard references must always contain valid references to other objects. When the referenced object is deleted the value of the parameter must be updated and the reference must be removed.
   This can be achieved by using `check_is_object_ref` in combination with `set_object_ref`. This write action handler will subscribe to monitor the referenced object, when the referenced object is removed an event will be recieved, and the reference will be removed. The subscription will be stored in the private data pointer of the parameter, so when using `set_object_ref` write handler no other private data can be stored. As the subscription must be closed again when the parameter containing the reference is deleted a destroy action handler is needed. This module provides this as well: `destroy_object_ref`.<br>

   Example:
   ```
   string Interface {
       on action validate call check_maximum_length 256;
       on action validate call matches_regexp "^$|^Device\.(IP|Logical)\.Interface\.([^\.]*|\[.*\])+\.?$";
       on action validate call check_is_object_ref;
       on action write call set_object_ref;
       on action destroy call destroy_object_ref;
   }
   ```

---
> **RECOMANDATION**<br>
> When parameters are referencing objects from other processes it is recommended to make sure that the process that provides these objects is running and its data model is available. In odl files this can be accomplished by using the `requires` keyword.
---

#### Path Transformations

When the reference parameter is referencing an object from the TR181 data model (starting with `Device.`) path transformations can be defined.

Path transformation MUST be used when the referenced object is in the same data model (local datamodel) as the parameter referencing it and the reference path starts with `Device.`. It is recommended to use path transformations for all other references when the referenced path starts with `Device.`.

Path transformation can be set on all action handlers, but there is no need to define the same transformation on all used action handlers. Typically it is only defined on the validation action. All other action handlers will be able to use the same transformation definition. 

Advantages of providing a path transformation:
- It is possible to reference an object in the same process by using the `Device.` prefix. If no transformation is provided a timeout could occur as the process will be waiting to get a response from itself
- Performance will be better as the TR181 data model mapper/proxy is skipped in the communication.

When path transformations are defined, the read action handler `read_object_ref` can become very handy as it will make sure that when reading the data in `public` or `protected` access mode the full path is given while reading the parameter in `private` access mode the transformed path is returned.


Example 1:

```
%define {
    object DynamicDNS {
        object Server[] {
           ...
        }

        object Client[] {
            ... 

            string Server {
                on action validate call check_is_object_ref { "'Device.DynamicDNS.'" = "DynamicDns." };
                on action write call set_object_ref;
                on action destroy call destroy_object_ref;
            }

            ...
        }
    }
}
```

In above example, if the `Server` parameter of `DynamicDNS.Client.{i}` contains a path starting with `Device.DynamicDNS.` it will be transformed to a path starting with `DynamicDNS.`, when checking if the referenced object exists. The value that has been set will not be changed. Here the parameter references an object that will be available in the same data model, by providing the transformation data it will eliminate the need for communication over the used bus system, as the data can be accessed directly in the same data model.

Example 2:

```
%define {
    object UserInterface {
        
        ...

        object HTTPAccess[] {
           ...

           %persistent string Interface {
               on action validate call check_maximum_length 256;
               on action validate call matches_regexp "^$|^(Device\.)?(IP|Logical)\.Interface\.([^\.]*|\[.*\])+\.?$";
               on action validate call check_is_object_ref { "'Device.IP.'" = "IP.", "'Device.Logical.'" = "Logical." };
               on action write call set_object_ref;
               on action destroy call destroy_object_ref;
           }

           ...
        }
    }
}
```

In above example, if the `Interface` parameter of `UserInterface.HTTPAccess.{i}` contains a path starting with `Device.IP.` it will be transformed to a path starting with `IP.` and it contains a path starting with `Device.Logical.` it will be transformed to a path starting with `Logical.`, when checking if the referenced object exists. The value that has been set will not be changed. Using the transformation data the `Device.` data model will be skipped and messages will be sent directly to the correct process.

### Reference following

Using the reference following action implementations it is possible to make all parameters of the referenced object available in the object containing the reference, without the need of duplicating these parameters and their values.

To make the reference following work, the read, list, describe and write action of the object must be overridden.

This can be done in the odl file by, importing this module and setting all actions of the object containing the reference to the `follow_reference` function. As an argument the parameter name of the parameter containing the reference object path can be provided, if no parameter name is provided, it is assumed that the parameter name is `Reference`.

```odl
import "mod-dmext.so" as mod-dmext;

%define {
    object MyObject {
        on action any call follow_reference "MyRefParam";
    }
}
```

Example:
```odl
%define {
    object Top {
        object Type1[] {
            %unique %key string Alias;
            string T1Text;
            int32 T1Number;
        }
        object Type2[] {
            %unique %key string Alias;
            bool Enable;
            string Flags;
            uint32 T2Number;
        }
        object Node[] {
            on action any call follow_reference "Reference";
            
            %unique %key string Alias;
            string Reference = "";
            string NodeParameter = "";
        }
    }
}
```

In the above example the `Node` object contains a parameter `Reference`. When this reference contains an object path to an instance of `Type1` or `Type2` all the parameters of the referenced object will become available (visible) in the instance of the `Node` object. The parameters of the `Node` instance will also still be available. When there is a conflict in parameter names (that is when both the local object as the referenced object contain the same parameters), the parameters of the local object get precedence.

It is possible to change the values of the referenced object through the object that contains the reference.

When the reference parameter contains a path to a non-existing object, only the parameters of the local object are visible.

---
> **Limitations**<br>
>
>- Reference following only works when the referenced object is in the same process (same data model).
>- Events are only send for the object that has been changed.
>- No additional read/write/list/describe actions van be put on the object containing the reference. 
>- Only object paths can be used (search paths or parameter paths are not allowed).
---


### Ordered Instances

Many multi-instance objects (tables) in [TR-181](https://usp-data-models.broadband-forum.org/tr-181-2-14-1-usp.html) have ordered instances. These multi-instance objects have a parameter called `Order`. 

Some example objects from TR-181 data model:
- Device.DHCPv4.Client.{i}.ReqOption.{i}.
- Device.Bridge.Filter.{i}.
- Device.Firewall.Level.{i}.

In all these multi-instance objects the description of the `Order` parameter is the same:

Taken from `Device.Firewall.Level.{i}`
> Position of the Level entry for user interface display; levels can be presented according to an increasing or decreasing level of security.
>
> When this value is modified, if the value matches that of an existing entry, the Order value for the existing entry and all greater-valued Order entries is incremented to ensure uniqueness of this value. A deletion causes Order values to be compacted. When a value is changed, incrementing occurs before compaction.
>
> The value of Order on creation of a Level table entry MUST be one greater than the largest current value.

This module provides the implementation of the described behavior, all you need to do is bind the implementation to your data model definition in the odl file.

Example `Device.Bridge.Filter.{i}.`

```
import "mod-dmext.so" as "tr181";

%define {
    object Bridging {
        object Filter[] {
            on action add-inst call add_ordered_instance;
            on action destroy call remove_ordered_instance;

            %unique %key string Alias;
            uint32 Order {
                on action validate call check_order_range;
            }
        }
    }
}

%populate {
    on event "dm:object-changed" call order_changed
        filter 'path matches "Bridging\.Filter\.[0-9]+\." &&
                contains("parameters.Order")';
}

%define {
    entry-point tr181.dmext_main;
}
```

To use  the ordered instances feature, all you need to do is import this module, and make sure it's entry point is called.

For all multi-instance objects that have a `Order` parameter which must behave as described in the TR-181 specification two actions must be added to the multi-instance object:

1. `add-inst` action must be mapped to the `add_ordered_instance` function. This function will make sure that the value of the `Order` parameter is correctly set if not provided or when provided that all other instances will be updated accordingly (increment).

2. `destroy` action must be mapped to the `removed_ordered_instance` function. This function will make sure that the `Order` value of all other instances are correctly modified (compact).

To make sure that `Order` changes are taken into account an event handler with filter must be added as well. When a `dm:object-changed` event occurs on the multi-instance object and the parameter `Order` is changed, the function `order_changed` must be called. This function will make sure the `Order` value of all other instances are correctly updated (increment followed by compact).

Optionally a validation action can be added to the `Order` parameter itself. A validation function is available to verify that the new set `Order` is in the allowed range (1 - number of instances).

This implementation will make sure that the instances are sorted according the `Order` value. This can be handy when looping over all the instances using `amxd_object_for_each(instance, ...)` macro.

Example:

```
amxd_object_t* filter = amxd_dm_findf(dm, "Bridge.Filter.");

amxd_object_for_each(instance, it, filter) {
    amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
    printf("Order = %d\n", amxd_object_get_value(uint32_t, inst, "Order", NULL));
}
```

Assuming there are 4 instances, the above code would give this output:

```
Order = 1
Order = 2
Order = 3
Order = 4
```

![Ordered instances in action](examples/ordered-instances.mp4)

#### Order parameter and events

When an order parameter value is changed this change can trigger an event. All orders that are change as a consequence if one instance of which the order has been changed will not be evented.

This is also true when an instance is added, the instance addition can trigger an event, while the order changes of all others will not.

When an instance is deleted, this instance deletion can trigger an event, while the order changes, if any, of all other instance will not.

### Parameter read from or write to file

Example:

```
%define {
    object NAT {
        %volatile int32 ParamInt {
            on action read call read_from_file {path = "/path/to/existing/file"};
            on action write call write_to_file {path = "/path/to/existing/file"};
        }
    }
}

%populate {
    object NAT {
        parameter ParamInt = 500;
    }
}
```

The above code will read from and write to file "file" in directory "/path/to/existing". The behavior is defined for regular files and files of the proc file system. 

#### read_from_file
This read action handler will return an error if the file cannot be opened e.g. because it doesn't exist or because of permissions, or if it cannot convert the file data to the type of the parameter. It will read at most the first 63 bytes. File changes do not result in dm:object-changed events (there is no inotify or polling).

#### write_to_file
This write action handler will return an error if the file cannot be opened e.g. because it doesn't exist or because of permissions. The default value of a parameter in ODL section %define is not written to the file, use %populate.

#### parameter attributes
- The parameter can have a combination of attributes %volatile and %persistent, see doc libamxo.
- Use %read-only if the parameter has action handler "read_from_file" but not action handler "write_to_file". Writing to a parameter with action handler "read_from_file" but without action handler "write_to_file" is undefined behavior.
- Parameter attributes %key and %unique are not supported.

### Rotate Max Instances
Many multi-instance objects may require limiting the number of instances they can contain. The rotate_max_instances function allows adding new instances while maintaining a maximum limit on the number of instances.

#### Basic Usage

```
%define {
    object <ObjectName> {
        object <InstanceName>[] {
            on action add-inst call rotate_max_instances {max-instances = <MaxInstances>};
            <ParameterType> <ParameterName>;
            ...
        }
    }
}
```

- `max-instances` is optional. If not set, default limit is 10 instances.

#### Description

When adding a new instance using rotate_max_instances, if the specified maximum number of instances (max-instances) is reached before adding the new instance, the function deletes the oldest instance to make room for the new one.

### Rotate Sorted Max Instances
The rotate_sorted_max_instances function is a variant of rotate_max_instances that sorts the instances based on a specified parameter before rotating.

#### Basic Usage

```
%define {
    object <ObjectName> {
        object <InstanceName>[] {
            on action add-inst call rotate_sorted_max_instances {max-instances = <MaxInstances>, sort-by = "<SortParameter>"};
            <ParameterType> <ParameterName>;
            ...
        }
    }
}
```
- `max-instances` is optional. If not set, default limit is 10 instances.
- `sort-by` is optional.

#### Description

When adding a new instance using rotate_sorted_max_instances, the function first sorts the instances based on the specified parameter (sort-by) and then applies the rotation logic similar to rotate_max_instances.

#### Example

```
%define {
    object Stats {
        object Stat[] {
            on action add-inst call rotate_sorted_max_instances {max-instances = 3, sort-by = "Time"};
            counted with StatNumberNumberOfEntries;
            %unique %key string Alias;
            %read-only datetime Time;
        }
    }
}
```

In this example, the Stats object contains instances of Stat, and when adding a new instance, rotate_sorted_max_instances is called. It limits the instances to a maximum of 3 and sorts them based on the Time parameter before rotating when adding a new instance.

This functionality is useful for managing multi-instance objects with a limited capacity and ensuring that the most relevant instances are retained.

