/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "dmext_priv.h"
#include "test_object_redirect.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "./reference.odl";

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_follow_reference_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "follow_reference", AMXO_FUNC(_follow_reference)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _dmext_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_follow_reference_teardown(UNUSED void** state) {
    _dmext_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_set_reference(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "Top.Node.1.");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "Top.Type1.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_read_result_contains_referenced_obj_params(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    assert_non_null(node);

    assert_int_equal(amxd_object_get_params(node, &data, amxd_dm_access_public), 0);
    amxc_var_dump(&data, STDOUT_FILENO);

    assert_non_null(GET_ARG(&data, "T1Number"));
    assert_non_null(GET_ARG(&data, "T1Text"));
    assert_string_equal(GET_CHAR(&data, "Alias"), "cpe-Node-1");

    amxc_var_clean(&data);
}

void test_referenced_parameters_are_not_persistent(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    assert_non_null(node);

    assert_int_equal(amxd_object_describe(node, &data, AMXD_OBJECT_ALL, amxd_dm_access_public), 0);
    amxc_var_dump(&data, STDOUT_FILENO);

    assert_false(GETP_BOOL(&data, "parameters.T1Text.attributes.persistent"));

    amxc_var_clean(&data);
}

void test_read_fails_if_unknown_parameter(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    assert_non_null(node);

    assert_int_not_equal(amxd_object_get_param(node, "Unknown", &data), 0);
    amxc_var_dump(&data, STDOUT_FILENO);
    assert_true(amxc_var_is_null(&data));

    amxc_var_clean(&data);
}

void test_can_set_parameter(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, "Test");
    assert_non_null(node);

    assert_int_equal(amxd_object_set_param(node, "Child", &data), 0);

    amxc_var_clean(&data);
}

void test_can_set_referenced_parameter(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, "Test");
    assert_non_null(node);

    assert_int_equal(amxd_object_set_param(node, "T1Text", &data), 0);

    amxc_var_clean(&data);
}

void test_set_fails_for_unknown_parameter(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, "Test");
    assert_non_null(node);

    assert_int_not_equal(amxd_object_set_param(node, "NotExisting", &data), 0);

    amxc_var_clean(&data);
}

void test_can_set_reference_to_non_existing_obj(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "Top.Node.1.");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "Top.Type2.2.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_set_fails_if_ref_obj_not_found(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    amxc_var_set(cstring_t, &data, "Test");
    assert_non_null(node);

    assert_int_not_equal(amxd_object_set_param(node, "Flags", &data), 0);

    amxc_var_clean(&data);
}

void test_can_read_if_ref_obj_not_found(UNUSED void** state) {
    amxc_var_t data;
    amxd_object_t* node = amxd_dm_findf(&dm, "Top.Node.1.");

    amxc_var_init(&data);
    assert_non_null(node);

    assert_int_equal(amxd_object_get_params(node, &data, amxd_dm_access_public), 0);
    amxc_var_dump(&data, STDOUT_FILENO);

    assert_string_equal(GET_CHAR(&data, "Alias"), "cpe-Node-1");

    amxc_var_clean(&data);
}
