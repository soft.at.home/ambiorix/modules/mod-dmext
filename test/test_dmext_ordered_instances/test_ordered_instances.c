/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "dmext_priv.h"
#include "dmext_ordered_instances.h"
#include "test_ordered_instances.h"

typedef uint32_t test_order_t[10];

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "./ordered.odl";

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

static bool verify_instances_are_in_order(amxd_object_t* filter, test_order_t* expected_order) {
    bool rv = false;
    uint32_t iter = 1;

    amxd_object_for_each(instance, it, filter) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        uint32_t index = amxd_object_get_index(inst);
        uint32_t order = amxd_object_get_value(uint32_t, inst, "Order", NULL);
        const char* alias = GET_CHAR(amxd_object_get_param_value(inst, "Alias"), NULL);

        printf("Alias = %s\n", alias);
        fflush(stdout);

        if(order != iter) {
            print_error(" Instance(%s) Unexpected Order: expected=%d actual=%d\n", alias, iter, order);
            goto exit;
        }

        if((*expected_order != NULL) && ((*expected_order)[iter - 1] != index)) {
            print_error(" Instance(%s) Unexpected Index: expected=%d actual=%d\n", alias, (*expected_order)[iter - 1], index);
            goto exit;
        }

        iter++;
    }

    rv = true;
exit:
    return rv;
}

int test_ordered_instances_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "add_ordered_instance", AMXO_FUNC(_add_ordered_instance)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "remove_ordered_instance", AMXO_FUNC(_remove_ordered_instance)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_order_range", AMXO_FUNC(_check_order_range)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "order_changed", AMXO_FUNC(_order_changed)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _dmext_main(0, &dm, &parser);

    handle_events();

    return 0;
}

int test_ordered_instances_teardown(UNUSED void** state) {
    _dmext_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_add_instances(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    assert_non_null(filter);

    amxd_trans_select_pathf(&trans, "Bridging.Filter.");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, NULL);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 2, 3, 4})));

    amxd_trans_clean(&trans);
}

void test_changing_order_keeps_instances_sorted(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    assert_non_null(filter);

    amxd_trans_select_pathf(&trans, "Bridging.Filter.3");
    amxd_trans_set_value(uint32_t, &trans, "Order", 2);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 3, 2, 4})));

    amxd_trans_clean(&trans);
}

void test_can_create_instance_with_order(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");
    amxd_object_t* new_instance = NULL;

    assert_non_null(filter);
    assert_null(amxd_object_get_instance(filter, NULL, 5));

    amxd_trans_select_pathf(&trans, "Bridging.Filter.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(uint32_t, &trans, "Order", 2);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    new_instance = amxd_object_get_instance(filter, NULL, 5);
    assert_non_null(new_instance);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 5, 3, 2, 4})));

    amxd_trans_clean(&trans);
}

void test_can_delete_instance(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    assert_non_null(filter);

    amxd_trans_select_pathf(&trans, "Bridging.Filter.");
    amxd_trans_del_inst(&trans, 3, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 5, 2, 4})));

    amxd_trans_clean(&trans);
}

void test_changing_order_and_removing_instance_does_not_segfault(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    assert_non_null(filter);

    amxd_trans_select_pathf(&trans, "Bridging.Filter.4");
    amxd_trans_set_value(uint32_t, &trans, "Order", 1);
    amxd_trans_select_pathf(&trans, "Bridging.Filter.");
    amxd_trans_del_inst(&trans, 4, NULL);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 5, 2})));

    amxd_trans_clean(&trans);
}

void test_set_order_out_of_range_fails(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    assert_non_null(filter);

    amxd_trans_select_pathf(&trans, "Bridging.Filter.1");
    amxd_trans_set_value(uint32_t, &trans, "Order", 999);

    assert_int_not_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    amxd_trans_clean(&trans);
}

void test_add_ordered_instance_adds_instance_if_needed(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");
    amxd_object_t* new_instance = NULL;

    assert_non_null(filter);
    assert_null(amxd_object_get_instance(filter, NULL, 6));

    assert_int_equal(amxd_object_remove_action_cb(filter, action_object_add_inst, amxd_action_object_add_inst), 0);
    amxd_trans_select_pathf(&trans, "Bridging.Filter.");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    new_instance = amxd_object_get_instance(filter, NULL, 6);
    assert_non_null(new_instance);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 5, 2, 6})));

    amxd_trans_clean(&trans);
}

void test_can_set_an_instance_to_the_last_in_order(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    amxd_trans_select_pathf(&trans, "Bridging.Filter.5");
    amxd_trans_set_value(uint32_t, &trans, "Order", 4);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    assert_true(verify_instances_are_in_order(filter, &((test_order_t) {1, 2, 6, 5})));

    amxd_trans_clean(&trans);
}

// MUST BE LAST TEST
void test_cleaning_dm_does_not_segfault(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* filter = amxd_dm_findf(&dm, "Bridging.Filter.");

    assert_non_null(filter);

    amxd_trans_select_pathf(&trans, "Bridging.Filter.1");
    amxd_trans_set_value(uint32_t, &trans, "Order", 3);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    amxd_dm_clean(&dm);
    handle_events();
    amxd_trans_clean(&trans);
}
