/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "dmext_priv.h"
#include "test_expr_validate.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "./expr_validate.odl";

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_expr_validate_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_expression", AMXO_FUNC(_check_expression)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_is_empty_or_in", AMXO_FUNC(_check_is_empty_or_in)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_is_empty_or_length_equal_to", AMXO_FUNC(_check_is_empty_or_length_equal_to)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    handle_events();

    return 0;
}

int test_expr_validate_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_check_expression_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Number", "65");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_check_expression_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Number", "58");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_check_is_in_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Controller", "mod-2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_check_is_empty_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Controller", "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_check_is_empty_or_in_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Controller", "mod-3");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_check_is_empty_or_length_equal_to_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates empty string
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Generic", "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);

    //Validates a 33-character string
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Generic", "This string is 33 characters long");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);

    //Validates a 40-character csv_string
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "GenericCSV", "19-character string, This is 19 as well!");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}

void test_check_is_empty_or_length_equal_to_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Invalidates a non-empty, non-33-character string
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "Generic", "This string is neither empty nor 33 characters long");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);

    //Invalidates a non-empty, non-40-character CSV string
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "GenericCSV", "These two strings are 40 characters each, combined as a single string they are not");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);

    handle_events();

    amxd_trans_clean(&trans);
}