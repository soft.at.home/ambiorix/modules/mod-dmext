/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "dmext_priv.h"
#include "test_local_object_reference.h"

#include <amxb/amxb_register.h>
#include <amxd/amxd_transaction.h>

static amxo_parser_t parser;
static amxd_dm_t dm;

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_local_object_reference_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    amxo_parser_init(&parser);
    amxd_dm_init(&dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_is_object_ref", AMXO_FUNC(_check_is_object_ref)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "set_object_ref", AMXO_FUNC(_set_object_ref)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "destroy_object_ref", AMXO_FUNC(_destroy_object_ref)), 0);

    root_obj = amxd_dm_get_root(&dm);
    amxo_parser_parse_file(&parser, "./test.odl", root_obj);

    handle_events();

    return 0;
}

int test_local_object_reference_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_odl_with_references(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxd_object_t* led = NULL;
    char* references = NULL;
    assert_int_equal(amxo_parser_parse_file(&parser, "./leds.odl", root_obj), 0);

    handle_events();

    led = amxd_dm_findf(&dm, "LEDs.LED.white.");
    assert_non_null(led);

    references = amxd_object_get_value(cstring_t, led, "Reference", NULL);
    assert_non_null(references);
    assert_string_equal(references, "Device.LEDs.LED.1,Device.LEDs.LED.2,Device.LEDs.LED.3");
    free(references);

    led = amxd_dm_findf(&dm, "LEDs.LED.overlay.");
    assert_non_null(led);

    references = amxd_object_get_value(cstring_t, led, "Reference", NULL);
    assert_non_null(references);
    assert_string_equal(references, "LEDs.LED.1");
    free(references);
}

void test_check_reference_is_cleared(UNUSED void** state) {
    amxd_object_t* led = NULL;
    char* references = NULL;

    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LEDs.LED.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    led = amxd_dm_findf(&dm, "LEDs.LED.white.");
    assert_non_null(led);

    references = amxd_object_get_value(cstring_t, led, "Reference", NULL);
    assert_non_null(references);
    assert_string_equal(references, "Device.LEDs.LED.2,Device.LEDs.LED.3");
    free(references);

    led = amxd_dm_findf(&dm, "LEDs.LED.overlay.");
    assert_non_null(led);

    references = amxd_object_get_value(cstring_t, led, "Reference", NULL);
    assert_non_null(references);
    assert_string_equal(references, "");
    free(references);
}

void test_check_reference_is_normalized(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxd_object_t* ipv6_addr = NULL;
    amxd_trans_t trans;
    char* references = NULL;

    assert_int_equal(amxo_parser_parse_file(&parser, "./ip.odl", root_obj), 0);
    handle_events();

    amxd_trans_init(&trans);

    ipv6_addr = amxd_dm_findf(&dm, "IP.Interface.wan.IPv6Address.1.");
    assert_non_null(ipv6_addr);

    amxd_trans_select_object(&trans, ipv6_addr);
    amxd_trans_set_cstring_t(&trans, "Prefix", "Device.IP.Interface.1.IPv6Prefix.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    references = amxd_object_get_value(cstring_t, ipv6_addr, "Prefix", NULL);
    assert_non_null(references);
    assert_string_equal(references, "Device.IP.Interface.1.IPv6Prefix.1");
    free(references);
}
