/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <errno.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "dmext_priv.h"
#include "test_read_write_file.h"

#include "../mocks/wrap_file_io.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "./read_write_file.odl";

static int mock_fd = 200;
static const char* mock_path_int = "/path/to/file1";
static const char* mock_path_str = "/path/to/file2";
extern const char* wrap_read_buffer;

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

static void _unit_test_integer_param_changed(UNUSED const char* const sig_name,
                                             const amxc_var_t* const event_data,
                                             UNUSED void* const priv) {
    amxc_var_t* var = GETP_ARG(event_data, "parameters.ParamInt");
    int from = 0;
    int to = 0;

    assert_non_null(var);

    from = GET_INT32(var, "from");
    check_expected(from);

    to = GET_INT32(var, "to");
    check_expected(to);
}

static void _unit_test_string_param_changed(UNUSED const char* const sig_name,
                                            const amxc_var_t* const event_data,
                                            UNUSED void* const priv) {
    amxc_var_t* var = GETP_ARG(event_data, "parameters.ParamStr");
    const char* from = "";
    const char* to = "";

    assert_non_null(var);

    from = GET_CHAR(var, "from");
    check_expected(from);

    to = GET_CHAR(var, "to");
    check_expected(to);
}

int test_read_write_file_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "unit_test_integer_param_changed", AMXO_FUNC(_unit_test_integer_param_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "unit_test_string_param_changed", AMXO_FUNC(_unit_test_string_param_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "read_from_file", AMXO_FUNC(_read_from_file)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "write_to_file", AMXO_FUNC(_write_to_file)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    handle_events();

    return 0;
}

int test_read_write_file_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_read_cant_open_file(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int value = 0;
    int original_value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    original_value = GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL);

    expect_string(__wrap_open, path, mock_path_int);
    errno = EACCES;
    will_return(__wrap_open, -1);

    value = amxd_object_get_value(int32_t, object, "ParamInt", &status);
    assert_int_equal(value, 0);
    assert_int_equal(status, amxd_status_ok);

    // just to be sure that the param value hasn't changed after failure
    assert_int_equal(GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL), original_value);
}

void test_cant_read_file(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int value = 0;
    int original_value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    original_value = GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    errno = EIO;
    will_return(__wrap_read, -1);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    value = amxd_object_get_value(int32_t, object, "ParamInt", &status);
    assert_int_equal(value, 0);
    assert_int_equal(status, amxd_status_ok);

    // just to be sure that the param value hasn't changed after failure
    assert_int_equal(GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL), original_value);
}

void test_read_integer_from_file(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "60000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    value = amxd_object_get_value(int32_t, object, "ParamInt", &status);
    assert_int_equal(value, 60000);
    assert_int_equal(status, amxd_status_ok);
}

void test_read_integer_truncated(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "1234567890";
    assert_true(strlen(wrap_read_buffer) > 63);
    will_return(__wrap_read, 63);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    value = amxd_object_get_value(int32_t, object, "ParamInt", &status);
    assert_int_equal(value, 012); // implementation detail: internal buffer can only read 63 bytes so wrap_read_buffer is truncated to 63 chars "00...012"
    assert_int_equal(status, amxd_status_ok);
}

void test_read_integer_conversion_failure(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int value = 0;
    int original_value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    original_value = GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "abcxyz";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    value = amxd_object_get_value(int32_t, object, "ParamInt", &status);
    assert_int_equal(value, 0);
    assert_int_equal(status, amxd_status_ok);

    // just to be sure that the param value hasn't changed after failure
    assert_int_equal(GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL), original_value);
}

void test_read_string_truncated(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    char* value = NULL;
    const char* expected = "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "12";

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    expect_string(__wrap_open, path, mock_path_str);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "0000000000"
        "1234567890";
    assert_true(strlen(wrap_read_buffer) > 63);
    will_return(__wrap_read, 63);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    value = amxd_object_get_value(cstring_t, object, "ParamStr", &status);

    assert_string_equal(value, expected); // implementation detail: internal buffer can only read 63 bytes so wrap_read_buffer is truncated to 63 chars "00...012"
    assert_int_equal(status, amxd_status_ok);
    free(value);
}

void test_write_cant_open_file(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    int original_value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    original_value = GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL);

    expect_string(__wrap_open, path, mock_path_int);
    errno = EACCES;
    will_return(__wrap_open, -1);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(int32_t, &trans, "ParamInt", 5000);
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    // just to be sure that the param value hasn't changed after failure
    assert_int_equal(GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL), original_value);
}

void test_cant_write_file(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    int original_value = 0;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    original_value = GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "6000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);
    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_write, fd, mock_fd);
    expect_string(__wrap_write, buf, "5000");
    errno = EIO;
    will_return(__wrap_write, -1);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(int32_t, &trans, "ParamInt", 5000);
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    // just to be sure that the param value hasn't changed after failure
    assert_int_equal(GET_INT32(amxd_object_get_param_value(object, "ParamInt"), NULL), original_value);
}

void test_write_same_value_as_read(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    object = amxd_dm_findf(&dm, "NAT.");
    assert_non_null(object);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "5000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    // important because amxrt uses this event to write the saved odl (for param attribute == "persistent and !volatile")
    expect_value(_unit_test_integer_param_changed, from, 60000);
    expect_value(_unit_test_integer_param_changed, to, 5000);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(int32_t, &trans, "ParamInt", 5000);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_write_integer_to_file(UNUSED void** state) {
    amxd_trans_t trans;

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "6000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);
    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_write, fd, mock_fd);
    expect_string(__wrap_write, buf, "5000");
    will_return(__wrap_write, 0);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    // important because amxrt uses this event to write the saved odl (for param attribute == "persistent and !volatile")
    expect_value(_unit_test_integer_param_changed, from, 60000);
    expect_value(_unit_test_integer_param_changed, to, 5000);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(int32_t, &trans, "ParamInt", 5000);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "6000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);
    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_write, fd, mock_fd);
    expect_string(__wrap_write, buf, "5001");
    will_return(__wrap_write, 0);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    // important because amxrt uses this event to write the saved odl (for param attribute == "persistent and !volatile")
    expect_value(_unit_test_integer_param_changed, from, 5000);
    expect_value(_unit_test_integer_param_changed, to, 5001);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(int32_t, &trans, "ParamInt", 5001);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_write_string_to_file(UNUSED void** state) {
    amxd_trans_t trans;

    expect_string(__wrap_open, path, mock_path_str);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "6000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);
    expect_string(__wrap_open, path, mock_path_str);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_write, fd, mock_fd);
    expect_string(__wrap_write, buf, "5000");
    will_return(__wrap_write, 0);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    // important because amxrt uses this event to write the saved odl (for param attribute == "persistent and !volatile")
    expect_string(_unit_test_string_param_changed, from, "Foo");
    expect_string(_unit_test_string_param_changed, to, "5000");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "ParamStr", "5000");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    expect_string(__wrap_open, path, mock_path_str);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "6000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);
    expect_string(__wrap_open, path, mock_path_str);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_write, fd, mock_fd);
    expect_string(__wrap_write, buf, "5001");
    will_return(__wrap_write, 0);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    // important because amxrt uses this event to write the saved odl (for param attribute == "persistent and !volatile")
    expect_string(_unit_test_string_param_changed, from, "5000");
    expect_string(_unit_test_string_param_changed, to, "5001");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(cstring_t, &trans, "ParamStr", "5001");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_load_default_odl(UNUSED void** state) {
    const char* odl = "%populate {"
        "    object NAT {"
        "        parameter ParamInt = 500;"
        "    }"
        "}";
    amxd_object_t* root = NULL;

    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_read, fd, mock_fd);
    wrap_read_buffer = "6000";
    will_return(__wrap_read, strlen(wrap_read_buffer));
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);
    expect_string(__wrap_open, path, mock_path_int);
    will_return(__wrap_open, mock_fd);
    expect_value(__wrap_write, fd, mock_fd);
    expect_string(__wrap_write, buf, "500");
    will_return(__wrap_write, 0);
    expect_value(__wrap_close, fd, mock_fd);
    will_return(__wrap_close, 0);

    root = amxd_dm_get_root(&dm);
    assert_int_equal(amxo_parser_parse_string(&parser, odl, root), 0);

    // important because amxrt uses this event to write the saved odl (for param attribute == "persistent and !volatile")
    expect_value(_unit_test_integer_param_changed, from, 60000);
    expect_value(_unit_test_integer_param_changed, to, 500);

    handle_events();
}
