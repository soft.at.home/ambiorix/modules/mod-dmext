/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "dmext_priv.h"
#include "test_ip_validation.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "./ip_validation.odl";

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_ip_validation_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_ipv6", AMXO_FUNC(_is_valid_ipv6)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_ipv6_prefix", AMXO_FUNC(_is_valid_ipv6_prefix)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_ipv4", AMXO_FUNC(_is_valid_ipv4)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_ipv4_network", AMXO_FUNC(_is_valid_ipv4_network)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_ip", AMXO_FUNC(_is_valid_ip)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_ip_range", AMXO_FUNC(_is_valid_ip_range)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_macaddr", AMXO_FUNC(_is_valid_macaddr)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_valid_hostname", AMXO_FUNC(_is_valid_hostname)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    handle_events();

    return 0;
}

int test_ip_validation_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_check_ipv6_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IPv6 Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:5678:9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:0:0:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv6 Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6AddressCSV", "1234:0:0:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6AddressCSV", "1234:0:0:DEF0::, 1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_ipv6_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IPv6 Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234::9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:0:0:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321:1234");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "12345::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:ABCD:EFGH::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Address", "1234:0:0:DEF0::, 1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv6 Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6AddressCSV", "1234::9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6AddressCSV", "1234:0:0:DEF0::, 12345::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_ipv6_prefix_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IPv6 Prefix
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234:5678:9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234:0:0:DEF0::/0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321/128");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv6 Prefix
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6PrefixCSV", "1234:5678:9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6PrefixCSV", "1234:5678:9ABC:DEF0::/64, 1234:0:0:DEF0::/0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6PrefixCSV",
                         "1234:5678:9ABC:DEF0::/64, 1234:0:0:DEF0::/0, 1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321/128 ");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

}

void test_check_ipv6_prefix_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IPv6 Prefix
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234::9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234:0:0:DEF0::/a");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321/129");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "12345::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix", "1234:ABCD:EFGH::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6Prefix",
                         "1234:5678:9ABC:DEF0::/64, 1234:0:0:DEF0::/0, 1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321/128 ");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv6 Prefix
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6PrefixCSV", "1234::9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6PrefixCSV", "1234:5678:9ABC:DEF0::/64, 1234::9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

}

void test_check_ipv4_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IPv4 Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Address", "127.0.0.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Address", "192.168.1.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Address", "172.17.0.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv4 Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4AddressCSV", "127.0.0.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4AddressCSV", "127.0.0.1, 192.168.1.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_ipv4_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IPv4 Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Address", "192.168.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Address", "192.168.1.256");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Address", "192.168.1.1, 172.17.0.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv4 Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4AddressCSV", "192.168.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4AddressCSV", "192.168.1.1, 192.168.1.256");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_ipv4_network_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "192.168.1.1/24");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "172.10.0.37/16");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "10.92.255.5/11");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "8.8.8.8/32");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv4 Subnets
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4SubnetCSV", "192.168.1.1/24, 172.10.0.37/16");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4SubnetCSV", "8.8.8.8/32, 10.0.10.0/8");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_ipv4_network_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "192.168.256.1/24");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "172.10.0.37/33");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "10..92.255.5/11");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4Subnet", "10.80.255/11");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IPv4 Subnets
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4SubnetCSV", "192.168.1.0/16, 192.168.256.10/24");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPv4SubnetCSV", "10.0.8.0/33, 172.10.0.0/32");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_ip_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle IP Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:5678:9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:0:0:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "127.0.0.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "172.17.0.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IP Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "1234:5678:9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "1234:5678:9ABC:DEF0::, 1234:0:0:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "192.168.1.1, 172.17.0.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

}

void test_check_ip_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a single IP Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234::9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:0:0:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:5678:9ABC:DEF0:0DEF:CBA9:8765:4321:1234");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "12345::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:ABCD:EFGH::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1.256");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1234:5678:9ABC:DEF0::, 1234:0:0:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IP Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "1234::9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "1234:5678:9ABC:DEF0::, 1234::9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "192.168.1.256");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddressCSV", "192.168.1.1, 192.168.1.256");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_iprange_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a single IP Range
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "192.168.1.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "1234:5678:9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "192.168.1.1/24");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "1234:5678:9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IP Ranges
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "10.0.1.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "1234:5678:9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "1234:5678:9ABC:DEF0::, 192.168.234.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "10.0.1.2/32");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "1234:5678:9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "172.16.10.2/16, 1234:5678:9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "10.0.1.1, 1234:0:0:DEF0::, 172.16.10.1/24, 1234:0:0:DEF0::/0");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_iprange_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a single IP Range
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "192.168.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "1234::9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "192.168.256.1/24");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "1234::9ABC:DEF0::/64");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "abc/24");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "10.0.1.1/");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRange", "192.168.1.1/abc");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV IP Ranges
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "192.168.256.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "1234:ABCD:EFGH::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "192.168.256.3, 1234:5678:9ABC:DEF0::");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "192.168.1.1/33");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "1234:5678:9ABC:DEF0::/129");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "192.168.1.1/30, 1234:ABCD:EFGH::/128");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "IPRangeCSV", "10.0.1.1, 1234:5678:9ABC:DEF0::, 172.16.256.1/24, 1234:5678:9ABC:DEF0::/128");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_mac_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle MAC Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "AA:BB:CC:11:22:33");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "44:56:78:DD:EF:1F");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV MAC Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddressCSV", "AA:BB:CC:11:22:33");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddressCSV", "AA:BB:CC:11:22:33, 44:56:78:DD:EF:1F");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

}

void test_check_mac_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a sigle MAC Address
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "AA:CC:11:22:33");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "44:XY:78:DD:EF:1F");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddress", "AA:BB:CC:11:22:33, 44:56:78:DD:EF:1F");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV MAC Addresses
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddressCSV", "AA:CC:11:22:33");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "MACAddressCSV", "AA:BB:CC:11:22:33, 44:XY:78:DD:EF:1F");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

}

void test_check_hostname_valid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a single hostname
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "sub-domain.example.co.tn");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "example.com");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "はじめよう.みんな");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "FD0D:86FA:C3BC::1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV hostnames
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostNameCSV", "192.168.1.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostNameCSV", "192.168.1.1, localhost");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_check_hostname_invalid_value(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_object_t* node = amxd_dm_findf(&dm, "MyObject.");

    assert_non_null(node);

    //Validates a single hostname
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "-example.com");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "example@com");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "192.168.1.1, localhost");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostName", "192.300.1.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    //Validates CSV hostnames
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostNameCSV", "ex ample.com");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MyObject.");
    amxd_trans_set_value(cstring_t, &trans, "HostNameCSV", "localhost, example!com");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_invalid_value);
    handle_events();
    amxd_trans_clean(&trans);
}
