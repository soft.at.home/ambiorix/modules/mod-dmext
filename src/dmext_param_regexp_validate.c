/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <regex.h>

#include "dmext_priv.h"
#include "dmext_assert.h"

const char* IPv4_regexp =
    "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";

const char* IPv6_regexp =
    "^("
    "([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|"
    "([0-9a-fA-F]{1,4}:){1,7}:|"
    "([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|"
    "([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|"
    "([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|"
    "([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|"
    "[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|"
    ":((:[0-9a-fA-F]{1,4}){1,7}|:)|"
    "fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|"
    "::(ffff(:0{1,4}){0,1}:){0,1}"
    "((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}"
    "(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|"
    "([0-9a-fA-F]{1,4}:){1,4}:"
    "((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}"
    "(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])"
    ")$";

const char* macaddr_regexp =
    "^([0-9a-fA-F]{2}:){5}([0-9a-fA-F]{2})$";

const char* hostname_regexp = //RFC 1035 & RFC 1123
    "^["
    "A-Za-z"                  //Basic Latin
    "À-ÖØ-öø-ÿ"               //Latin-1 Supplement
    "Ā-ɏ"                     //Latin extended A and B
    "\u1E00-\u1EFF"           //Latin Extended Additional
    "\u0370-\u03FF"           //Greek and Coptic
    "\u0400-\u04FF"           //Cyrillic
    "\u0531-\u058F"           //Armenian
    "\u0591-\u05F4"           //Hebrew
    "\u0600-\u06FF"           //Arabic
    "\u0900-\u097F"           //Devanagari
    "\u3041-\u30FF"           //Hiragana & Katakana
    "\uAC00-\uD7AF"           //Hangul
    "\u4E00-\u9FFF"           //Chinese, Japanese, Korean (CJK) Unified Ideographs
    "0-9]"
    "([A-Za-zÀ-ÖØ-öø-ÿĀ-ɏ\u1E00-\u1EFF\u0370-\u03FF\u0400-\u04FF\u0531-\u058F\u0591-\u05F4\u0600-\u06FF\u0900-\u097F\u3041-\u30FF\uAC00-\uD7AF\u4E00-\u9FFF0-9-]{0,61}"
    "[A-Za-zÀ-ÖØ-öø-ÿĀ-ɏ\u1E00-\u1EFF\u0370-\u03FF\u0400-\u04FF\u0531-\u058F\u0591-\u05F4\u0600-\u06FF\u0900-\u097F\u3041-\u30FF\uAC00-\uD7AF\u4E00-\u9FFF0-9])?"
    "(\\.[A-Za-zÀ-ÖØ-öø-ÿĀ-ɏ\u1E00-\u1EFF\u0370-\u03FF\u0400-\u04FF\u0531-\u058F\u0591-\u05F4\u0600-\u06FF\u0900-\u097F\u3041-\u30FF\uAC00-\uD7AF\u4E00-\u9FFF0-9]"
    "([A-Za-zÀ-ÖØ-öø-ÿĀ-ɏ\u1E00-\u1EFF\u0370-\u03FF\u0400-\u04FF\u0531-\u058F\u0591-\u05F4\u0600-\u06FF\u0900-\u097F\u3041-\u30FF\uAC00-\uD7AF\u4E00-\u9FFF0-9-]{0,61}"
    "[A-Za-zÀ-ÖØ-öø-ÿĀ-ɏ\u1E00-\u1EFF\u0370-\u03FF\u0400-\u04FF\u0531-\u058F\u0591-\u05F4\u0600-\u06FF\u0900-\u097F\u3041-\u30FF\uAC00-\uD7AF\u4E00-\u9FFF0-9])?)*"
    "\\.?"                    //fully qualified domain names commonly end with a '.'
    "$";

const char* IPv4_format_regex = "^([0-9]+\\.){3}[0-9]+$";

static bool dmext_var_is_string(const amxc_var_t* const var) {
    int type = amxc_var_type_of(var);
    if((type == AMXC_VAR_ID_CSTRING) ||
       ( type == AMXC_VAR_ID_CSV_STRING) ||
       ( type == AMXC_VAR_ID_SSV_STRING)) {
        return true;
    }

    return false;
}

static amxd_status_t dmext_validate_regexp(const char* regexp_str, const char* value) {
    amxd_status_t status = amxd_status_unknown_error;
    regex_t regexp;

    int rv = regcomp(&regexp, regexp_str, REG_EXTENDED | REG_NOSUB);
    when_true_status(rv != 0, exit, status = amxd_status_invalid_arg);

    if(regexec(&regexp, value, 0, NULL, 0) == 0) {
        status = amxd_status_ok;
    } else {
        status = amxd_status_invalid_value;
    }

    regfree(&regexp);

exit:
    return status;
}

static amxd_status_t regexp_validate_all(amxd_param_t* param,
                                         const amxc_var_t* const new_value,
                                         const char* regexp) {
    uint32_t param_type = amxc_var_type_of(&param->value);
    amxd_status_t status = amxd_status_invalid_type;
    amxc_var_t values;

    amxc_var_init(&values);

    if(param_type == AMXC_VAR_ID_CSTRING) {
        amxc_var_convert(&values, new_value, param_type);
        when_str_empty_status(GET_CHAR(&values, NULL), exit, status = amxd_status_ok);
        status = dmext_validate_regexp(regexp, GET_CHAR(&values, NULL));
    } else if((param_type == AMXC_VAR_ID_CSV_STRING) ||
              (param_type == AMXC_VAR_ID_SSV_STRING)) {
        status = amxd_status_ok;
        amxc_var_convert(&values, new_value, param_type);
        amxc_var_cast(&values, AMXC_VAR_ID_LIST);
        amxc_var_for_each(value, &values) {
            status = dmext_validate_regexp(regexp, GET_CHAR(value, NULL));
            when_false(status == amxd_status_ok, exit);
        }
    }

exit:
    amxc_var_clean(&values);
    return status;
}

static amxd_status_t dmext_validate_prefix(const char* value) {
    amxd_status_t status = amxd_status_invalid_value;
    amxc_string_t value_amx_str;
    const char* length_str = NULL;
    int pos = -1;
    char* end_ptr = NULL;
    long length_i = -1;

    amxc_string_init(&value_amx_str, 0);

    when_str_empty_status(value, exit, status = amxd_status_ok);
    amxc_string_set(&value_amx_str, value);

    pos = amxc_string_search(&value_amx_str, "/", 0);
    when_false(pos > 0, exit);
    length_str = amxc_string_get(&value_amx_str, pos + 1);
    when_false(length_str && *length_str, exit);
    length_i = strtol(length_str, &end_ptr, 10);
    when_false(*end_ptr == '\0', exit);
    when_false(length_i >= 0 && length_i <= 128, exit);

    amxc_string_remove_at(&value_amx_str, pos, UINT32_MAX);
    status = dmext_validate_regexp(IPv6_regexp,
                                   amxc_string_get(&value_amx_str, 0));

exit:
    amxc_string_clean(&value_amx_str);
    return status;
}

static amxd_status_t dmext_validate_network(const char* value) {
    amxd_status_t status = amxd_status_invalid_value;
    amxc_string_t value_amx_str;
    const char* subnet_str = NULL;
    int pos = -1;
    char* end_ptr = NULL;
    long length_i = -1;

    amxc_string_init(&value_amx_str, 0);

    when_str_empty_status(value, exit, status = amxd_status_ok);
    amxc_string_set(&value_amx_str, value);

    pos = amxc_string_search(&value_amx_str, "/", 0);
    when_false(pos > 0, exit);
    subnet_str = amxc_string_get(&value_amx_str, pos + 1);
    when_false(subnet_str && *subnet_str, exit);
    length_i = strtol(subnet_str, &end_ptr, 10);
    when_false(*end_ptr == '\0', exit);
    when_false(length_i >= 0 && length_i <= 32, exit);

    amxc_string_remove_at(&value_amx_str, pos, UINT32_MAX);
    status = dmext_validate_regexp(IPv4_regexp,
                                   amxc_string_get(&value_amx_str, 0));

exit:
    amxc_string_clean(&value_amx_str);
    return status;
}

static amxd_status_t dmext_validate_ip_range(const char* value) {
    amxd_status_t status = amxd_status_invalid_value;
    when_null(value, exit);
    when_str_empty_status(value, exit, status = amxd_status_ok);

    status = dmext_validate_regexp(IPv4_regexp, value);
    when_true(status == amxd_status_ok, exit);
    status = dmext_validate_regexp(IPv6_regexp, value);
    when_true(status == amxd_status_ok, exit);
    status = dmext_validate_network(value);
    when_true(status == amxd_status_ok, exit);
    status = dmext_validate_prefix(value);

exit:
    return status;
}

amxd_status_t _matches_regexp(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* data = (amxc_var_t*) priv;
    const char* regexp_str = NULL;
    char* value_str = NULL;

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(data, exit, status = amxd_status_invalid_arg);
    when_true_status(!dmext_var_is_string(data), exit,
                     status = amxd_status_invalid_arg);

    status = amxd_action_describe_action(reason, retval, "matches_regexp", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    regexp_str = amxc_var_constcast(cstring_t, data);
    when_str_empty_status(regexp_str, exit, status = amxd_status_invalid_arg);

    value_str = amxc_var_dyncast(cstring_t, args);
    when_str_empty_status(value_str, exit, status = amxd_status_ok);

    status = dmext_validate_regexp(regexp_str, value_str);

exit:
    free(value_str);
    return status;
}

amxd_status_t _csv_values_matches_regexp(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* data = (amxc_var_t*) priv;
    const char* regexp_str = NULL;
    amxc_var_t csv;

    amxc_var_init(&csv);

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(data, exit, status = amxd_status_invalid_arg);
    when_true_status(!dmext_var_is_string(data), exit,
                     status = amxd_status_invalid_arg);

    status = amxd_action_describe_action(reason, retval, "csv_values_matches_regexp", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    regexp_str = amxc_var_constcast(cstring_t, data);
    when_str_empty_status(regexp_str, exit, status = amxd_status_invalid_arg);

    amxc_var_convert(&csv, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(value, &csv) {
        status = dmext_validate_regexp(regexp_str, GET_CHAR(value, NULL));
        when_false(status == amxd_status_ok, exit);
    }

exit:
    amxc_var_clean(&csv);
    return status;
}

amxd_status_t _is_valid_ipv4(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_ipv4", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = regexp_validate_all(param, args, IPv4_regexp);

exit:
    return status;
}

amxd_status_t _is_valid_ipv4_network(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    char* value_str = NULL;
    amxc_var_type_id_t param_type = AMXC_VAR_ID_NULL;
    amxc_var_t values;

    amxc_var_init(&values);

    when_null(param, exit);
    param_type = amxc_var_type_of(&param->value);

    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_ipv4_network", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    if(param_type == AMXC_VAR_ID_CSTRING) {
        value_str = amxc_var_dyncast(cstring_t, args);
        status = dmext_validate_network(value_str);
    } else if((param_type == AMXC_VAR_ID_CSV_STRING) ||
              (param_type == AMXC_VAR_ID_SSV_STRING)) {
        amxc_var_convert(&values, args, param_type);
        amxc_var_cast(&values, AMXC_VAR_ID_LIST);

        amxc_var_for_each(value, &values) {
            status = dmext_validate_network(GET_CHAR(value, NULL));
            when_failed(status, exit);
        }
    }

exit:
    amxc_var_clean(&values);
    free(value_str);
    return status;
}

amxd_status_t _is_valid_ipv6(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_ipv6", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = regexp_validate_all(param, args, IPv6_regexp);

exit:
    return status;
}

amxd_status_t _is_valid_ipv6_prefix(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    char* value_str = NULL;
    uint32_t param_type = -1;
    amxc_var_t values;

    amxc_var_init(&values);

    when_null(param, exit);
    param_type = amxc_var_type_of(&param->value);

    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_ipv6_prefix", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    if(param_type == AMXC_VAR_ID_CSTRING) {
        value_str = amxc_var_dyncast(cstring_t, args);
        status = dmext_validate_prefix(value_str);
    } else if((param_type == AMXC_VAR_ID_CSV_STRING) ||
              (param_type == AMXC_VAR_ID_SSV_STRING)) {
        amxc_var_convert(&values, args, param_type);
        amxc_var_cast(&values, AMXC_VAR_ID_LIST);

        amxc_var_for_each(value, &values) {
            status = dmext_validate_prefix(GET_CHAR(value, NULL));
            when_failed(status, exit);
        }
    }

exit:
    amxc_var_clean(&values);
    free(value_str);
    return status;
}

amxd_status_t _is_valid_ip(amxd_object_t* object,
                           amxd_param_t* param,
                           amxd_action_t reason,
                           const amxc_var_t* const args,
                           amxc_var_t* const retval,
                           void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_ip", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = regexp_validate_all(param, args, IPv4_regexp);
    if(status != amxd_status_ok) {
        status = regexp_validate_all(param, args, IPv6_regexp);
    }

exit:
    return status;
}

amxd_status_t _is_valid_ip_range(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    char* value_str = NULL;
    amxc_var_type_id_t param_type = AMXC_VAR_ID_NULL;
    amxc_var_t values;

    amxc_var_init(&values);

    when_null(param, exit);
    param_type = amxc_var_type_of(&param->value);

    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_ip_range", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    if(param_type == AMXC_VAR_ID_CSTRING) {
        value_str = amxc_var_dyncast(cstring_t, args);
        status = dmext_validate_ip_range(value_str);
    } else if((param_type == AMXC_VAR_ID_CSV_STRING) ||
              (param_type == AMXC_VAR_ID_SSV_STRING)) {
        amxc_var_convert(&values, args, param_type);
        amxc_var_cast(&values, AMXC_VAR_ID_LIST);

        amxc_var_for_each(value, &values) {
            status = dmext_validate_ip_range(GET_CHAR(value, NULL));
            when_false(status == amxd_status_ok, exit);
        }
    }

exit:
    amxc_var_clean(&values);
    free(value_str);
    return status;
}

amxd_status_t _is_valid_macaddr(amxd_object_t* object,
                                amxd_param_t* param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_macaddr", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = regexp_validate_all(param, args, macaddr_regexp);

exit:
    return status;
}

amxd_status_t _is_valid_hostname(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null(param, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "is_valid_hostname", priv);
    when_true(status == amxd_status_ok, exit);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = regexp_validate_all(param, args, IPv4_regexp);
    when_true(status == amxd_status_ok, exit);

    //Input is not valid ipv4 but could have ipv4 format. Rule those cases out
    status = regexp_validate_all(param, args, IPv4_format_regex);
    when_true_status(status == amxd_status_ok,
                     exit,
                     status = amxd_status_invalid_value);

    status = regexp_validate_all(param, args, IPv6_regexp);
    when_true(status == amxd_status_ok, exit);

    status = regexp_validate_all(param, args, hostname_regexp);

exit:
    return status;
}