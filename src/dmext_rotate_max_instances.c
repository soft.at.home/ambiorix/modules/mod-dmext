/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "dmext_priv.h"
#include "dmext_assert.h"

static const char* sort_by = NULL;

static bool dmext_instance_is_created(amxd_object_t* const object,
                                      amxc_var_t* data) {
    bool retval = false;

    when_false(amxc_var_type_of(data) == AMXC_VAR_ID_HTABLE, exit);
    when_null(GET_ARG(data, "index"), exit);
    when_null(amxd_object_get_instance(object, NULL, GET_UINT32(data, "index")), exit);

    retval = true;

exit:
    return retval;
}

static int dmext_sort_instances(amxc_llist_it_t* it1, amxc_llist_it_t* it2) {
    amxd_object_t* obj1 = amxc_container_of(it1, amxd_object_t, it);
    amxd_object_t* obj2 = amxc_container_of(it2, amxd_object_t, it);
    int retval = 0;
    amxc_var_t value1;
    amxc_var_t value2;

    amxc_var_init(&value1);
    amxc_var_init(&value2);

    amxd_object_get_param(obj1, sort_by, &value1);
    amxd_object_get_param(obj2, sort_by, &value2);

    amxc_var_compare(&value1, &value2, &retval);
    if(retval != 0) {
        if((amxc_var_type_of(&value1) == AMXC_VAR_ID_TIMESTAMP) ||
           (amxc_var_type_of(&value2) == AMXC_VAR_ID_TIMESTAMP)) {
            char* str1 = amxc_var_dyncast(cstring_t, &value1);
            char* str2 = amxc_var_dyncast(cstring_t, &value2);
            if((strcmp(str1, "0001-01-01T00:00:00Z") == 0) ||
               (strcmp(str2, "0001-01-01T00:00:00Z") == 0)) {
                retval *= -1;
            }
            free(str1);
            free(str2);
        }
    }

    amxc_var_clean(&value1);
    amxc_var_clean(&value2);

    return retval;
}

amxd_status_t _rotate_max_instances(amxd_object_t* const object,
                                    amxd_param_t* const p,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    bool is_created = false;
    uint32_t count = 0;
    amxc_var_t* max_data = (amxc_var_t*) priv;
    uint32_t max = GET_UINT32(max_data, NULL) == 0 ? 10 : GET_UINT32(max_data, NULL);

    when_true_status(reason != action_object_add_inst,
                     exit,
                     status = amxd_status_function_not_implemented);

    is_created = dmext_instance_is_created(object, retval);
    count = amxc_llist_size(&object->instances);
    if(is_created) {
        count--;
        status = amxd_status_ok;
    } else {
        status = amxd_action_object_add_inst(object, p, reason, args, retval, NULL);
    }

    if((count >= max) && (status == amxd_status_ok)) {
        amxc_llist_it_t* it = amxc_llist_get_first(&object->instances);
        amxd_object_t* oldest = amxc_container_of(it, amxd_object_t, it);
        amxd_object_emit_del_inst(oldest);
        amxd_object_delete(&oldest);
    }

exit:
    return status;
}

amxd_status_t _rotate_sorted_max_instances(amxd_object_t* const object,
                                           amxd_param_t* const p,
                                           amxd_action_t reason,
                                           const amxc_var_t* const args,
                                           amxc_var_t* const retval,
                                           void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* max_data = (amxc_var_t*) priv;

    when_true_status(reason != action_object_add_inst,
                     exit,
                     status = amxd_status_function_not_implemented);

    sort_by = GET_CHAR(max_data, "sort-by");
    amxc_llist_sort(&object->instances, dmext_sort_instances);
    sort_by = NULL;

    status = _rotate_max_instances(object, p, reason, args, retval, GET_ARG(max_data, "max-instances"));

exit:
    return status;
}