/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "dmext_priv.h"
#include "dmext_ordered_instances.h"

static void dmext_compact_order(amxd_object_t* templ_obj, uint32_t skip_index) {
    uint32_t order = 1;
    amxd_object_for_each(instance, it, templ_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        amxd_param_t* param = amxd_object_get_param_def(inst, "Order");

        if(amxd_object_get_index(inst) == skip_index) {
            continue;
        }
        amxc_var_set(uint32_t, &param->value, order);
        order++;
    }
}

static void dmext_increment_order(amxd_object_t* templ_obj, uint32_t index, uint32_t order) {
    amxd_object_for_each(instance, it, templ_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        uint32_t inst_order = 0;

        if(amxd_object_get_index(inst) == index) {
            continue;
        }

        inst_order = amxd_object_get_value(uint32_t, inst, "Order", NULL);
        if(order <= inst_order) {
            amxd_param_t* param = amxd_object_get_param_def(inst, "Order");
            amxc_var_set(uint32_t, &param->value, inst_order + 1);
        }
    }
}

static int dmext_cmp_order(amxc_llist_it_t* it1, amxc_llist_it_t* it2) {
    amxd_object_t* obj1 = amxc_container_of(it1, amxd_object_t, it);
    amxd_object_t* obj2 = amxc_container_of(it2, amxd_object_t, it);

    uint32_t order1 = amxd_object_get_value(uint32_t, obj1, "Order", NULL);
    uint32_t order2 = amxd_object_get_value(uint32_t, obj2, "Order", NULL);

    return order1 - order2;
}

static void dmext_sort_objects(amxd_object_t* templ_obj) {
    amxc_llist_sort(&templ_obj->instances, dmext_cmp_order);
}

static amxd_object_t* amxd_action_add_inst_is_created(amxd_object_t* const object,
                                                      amxc_var_t* data) {
    amxd_object_t* instance = NULL;

    when_false(amxc_var_type_of(data) == AMXC_VAR_ID_HTABLE, exit);
    when_null(GET_ARG(data, "index"), exit);
    instance = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));

exit:
    return instance;
}

amxd_status_t _add_ordered_instance(amxd_object_t* const object,
                                    amxd_param_t* const p,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* params = NULL;
    amxc_var_t* order_var = NULL;
    const amxc_var_t* margs = args;
    amxc_var_t aargs;
    uint32_t order = 0;
    uint32_t last = 0;
    amxd_object_t* instance = NULL;

    amxc_var_init(&aargs);
    when_null(object, exit);
    when_null(retval, exit);

    instance = amxd_action_add_inst_is_created(object, retval);

    last = amxc_llist_size(&object->instances);
    if(instance != NULL) {
        last--;
    }
    params = amxc_var_get_key(args, "parameters", AMXC_VAR_FLAG_DEFAULT);
    if(params == NULL) {
        amxc_var_copy(&aargs, args);
        margs = &aargs;
        params = amxc_var_add_key(amxc_htable_t, &aargs, "parameters", NULL);
    }
    order_var = GET_ARG(params, "Order");
    order = GET_UINT32(order_var, NULL);

    if((order_var == NULL) || (order > last + 1)) {
        order = last + 1;
        amxc_var_add_key(uint32_t, params, "Order", last + 1);
    }
    if(instance != NULL) {
        amxd_param_t* porder = amxd_object_get_param_def(instance, "Order");
        amxc_var_set(uint32_t, &porder->value, order);
        status = amxd_status_ok;
    } else {
        status = amxd_action_object_add_inst(object, p, reason, margs, retval, priv);
    }

    if(status == amxd_status_ok) {
        dmext_increment_order(object, GETP_UINT32(retval, "index"), order);
        dmext_sort_objects(object);
    }

exit:
    amxc_var_clean(&aargs);
    return status;
}

amxd_status_t _remove_ordered_instance(amxd_object_t* object,
                                       amxd_param_t* const p,
                                       amxd_action_t reason,
                                       const amxc_var_t* const args,
                                       amxc_var_t* const retval,
                                       void* priv) {
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* templ_obj = NULL;

    when_null(object, exit);
    when_false_status(reason == action_object_destroy,
                      exit,
                      status = amxd_status_function_not_implemented);

    if(amxd_object_get_type(object) == amxd_object_template) {
        status = amxd_action_object_destroy(object, p, reason, args, retval, priv);
    } else {
        templ_obj = amxd_object_get_parent(object);
        dmext_compact_order(templ_obj, amxd_object_get_index(object));
    }

    amxc_var_clean(retval);

exit:
    return status;
}

void _order_changed(UNUSED const char* const sig_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    amxd_dm_t* dm = dmext_get_dm();
    uint32_t order_from = GETP_UINT32(event_data, "parameters.Order.from");
    uint32_t order_to = GETP_UINT32(event_data, "parameters.Order.to");
    amxd_object_t* inst = amxd_dm_signal_get_object(dm, event_data);
    amxd_object_t* templ_obj = amxd_object_get_parent(inst);
    uint32_t index = amxd_object_get_index(inst);

    when_null(dm, exit);
    when_null(inst, exit);

    if(order_to != order_from) {
        if(order_to >= amxc_llist_size(&templ_obj->instances)) {
            amxd_param_t* param = amxd_object_get_param_def(inst, "Order");
            amxc_var_set(uint32_t, &param->value, order_to + 1);
        } else {
            dmext_increment_order(templ_obj, index, order_to);
        }

        dmext_sort_objects(templ_obj);
        dmext_compact_order(templ_obj, 0);
    }

exit:
    return;
}

amxd_status_t _check_order_range(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* templ_obj = NULL;
    amxc_var_t range;

    amxc_var_init(&range);
    amxc_var_set_type(&range, AMXC_VAR_ID_LIST);

    when_null(param, exit);
    when_null(object, exit);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_true_status(amxd_object_get_type(object) != amxd_object_instance,
                     exit,
                     status = amxd_status_ok);

    status = amxd_action_describe_action(reason, retval, "check_order_range", NULL);
    when_true(status == amxd_status_ok, exit);

    templ_obj = amxd_object_get_parent(object);
    amxc_var_add(uint32_t, &range, 1);
    amxc_var_add(uint32_t, &range, amxc_llist_size(&templ_obj->instances));
    status = amxd_action_param_check_range(object, param, reason, args, retval, &range);

exit:
    amxc_var_clean(&range);
    return status;
}
