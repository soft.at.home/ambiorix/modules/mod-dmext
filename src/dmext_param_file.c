/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "dmext_priv.h"
#include "dmext_assert.h"

static amxd_status_t dmext_file_read(char** out, int32_t fd) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_string_t text;
    char buffer[64] = {0};

    amxc_string_init(&text, sizeof(buffer));

    when_true_status(read(fd, buffer, sizeof(buffer) - 1) == -1, exit, status = amxd_status_invalid_value);
    amxc_string_set(&text, buffer);
    amxc_string_trim(&text, NULL);
    *out = amxc_string_take_buffer(&text);

    status = amxd_status_ok;
exit:
    amxc_string_clean(&text);
    return status;
}

amxd_status_t _read_from_file(UNUSED amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* file = (amxc_var_t*) priv;
    amxc_var_t param_value_backup;
    char* text = NULL;
    int32_t fd = -1;
    bool restore = false;

    amxc_var_init(&param_value_backup);

    when_null(param, exit);
    when_true_status(reason != action_param_read,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_failed_status(amxc_var_move(&param_value_backup, &param->value), exit, status = amxd_status_out_of_mem);
    restore = true;

    fd = open(GET_CHAR(file, "path"), O_RDONLY);
    when_true_status(fd == -1, exit, status = amxd_status_invalid_value);
    when_failed_status(dmext_file_read(&text, fd), exit, status = amxd_status_invalid_value);
    when_failed(amxc_var_push(cstring_t, &param->value, text), exit); // set param->value so that "dm:object-changed" event_data has correct "from" value
    text = NULL;

    when_failed_status(amxc_var_cast(&param->value, amxc_var_type_of(&param_value_backup)),
                       exit,
                       status = amxd_status_invalid_value); // keep the original type
    when_failed(amxc_var_copy(retval, &param->value), exit);

    status = amxd_status_ok;

exit:
    if((status != amxd_status_ok) && restore) {
        amxc_var_move(&param->value, &param_value_backup);
    }
    amxc_var_clean(&param_value_backup);
    if(fd != -1) {
        close(fd);
    }
    return status;
}

amxd_status_t _write_to_file(UNUSED amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* file = (amxc_var_t*) priv;
    amxc_var_t args_str;
    amxc_var_t param_value_backup;
    char* read_text = NULL;
    const char* text = NULL;
    int32_t fd = -1;
    bool restore = false;

    amxc_var_init(&args_str);
    amxc_var_init(&param_value_backup);

    when_null(param, exit);
    when_true_status(reason != action_param_write,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_failed_status(amxc_var_move(&param_value_backup, &param->value), exit, status = amxd_status_out_of_mem);
    restore = true;

    when_failed_status(amxc_var_convert(&param->value,
                                        args,
                                        amxc_var_type_of(&param_value_backup)),
                       exit,
                       status = amxd_status_invalid_value);

    fd = open(GET_CHAR(file, "path"), O_RDONLY);
    when_true_status(fd == -1, exit, status = amxd_status_invalid_value);
    when_failed_status(dmext_file_read(&read_text, fd), exit, status = amxd_status_invalid_value);
    close(fd);
    fd = -1;

    when_failed_status(amxc_var_convert(&args_str, args, AMXC_VAR_ID_CSTRING), exit, status = amxd_status_out_of_mem);
    text = GET_CHAR(&args_str, NULL);
    when_null(text, exit);
    when_true_status(strcmp(read_text, text) == 0, exit, status = amxd_status_ok); //skip write if the value is the same

    fd = open(GET_CHAR(file, "path"), O_WRONLY | O_SYNC);
    when_true_status(fd == -1, exit, status = amxd_status_invalid_value);
    when_true_status(write(fd, text, strlen(text)) == -1, exit, status = amxd_status_invalid_value);

    status = amxd_status_ok;

exit:
    if((status != amxd_status_ok) && restore) {
        amxc_var_move(&param->value, &param_value_backup);
    }

    free(read_text);
    amxc_var_clean(&args_str);
    amxc_var_clean(&param_value_backup);
    if(fd != -1) {
        close(fd);
    }
    return status;
}
